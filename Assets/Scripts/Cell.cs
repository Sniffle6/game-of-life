﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Cell
{
    public int State { get; set; }
    public GameObject gameObject { get; private set; }
    public int X;
    public int Y;
    public float timeChanged { get; private set; }
    public Renderer Renderer { get; private set; }
    public Cell(int state, int x, int y, GameObject prefab)
    {
        State = state;
        gameObject = Object.Instantiate(prefab, new Vector3(-(GOLmanager.Horizontal - 0.5f) + x, -(GOLmanager.Vertical - 0.5f) + y), Quaternion.identity);
        Renderer = gameObject.GetComponent<Renderer>();
        X = x;
        Y = y;
        timeChanged = Time.time;
    }

    public void SetState(int value)
    {
        State = value;
        timeChanged = Time.time;
    }
    public void FlipState()
    {
        timeChanged = Time.time;
        if (State == 0)
            State = 1;
        else
            State = 0;
    }
}










//texture = new Texture2D(1, 1);
//Color[] color = texture.GetPixels();
//for (int i = 0; i < color.Length; i++)
//{
//    color[i] = new Color(1, 1, 1);
//}
//texture.SetPixels(color);
//texture.Apply();
//State = state;

//sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 1.0f);
//spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
//spriteRenderer.material = mat;
//spriteRenderer.sprite = sprite;

//SetState(state);