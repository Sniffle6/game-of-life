﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOLmanager : MonoBehaviour
{
    public GameObject prefab;
    int Columns;
    int Rows;
    new Camera camera;
    [HideInInspector]
    public static int Horizontal;
    [HideInInspector]
    public static int Vertical;

    public Texture2D heightmap;
    AudioSource a_Source;
    Cell[,] Grid;
    int[,] NextGrid;
    int[,] staticCells;
    Cell tempCell;
    int maxCells;
    void Start()
    {
        a_Source = GetComponent<AudioSource>();
        camera = Camera.main;
        Setup();
    }
    void Setup()
    {
        Vertical = (int)camera.orthographicSize;
        Horizontal = Vertical * Screen.width / Screen.height;

        Columns = (Horizontal * 2);
        Rows = (Vertical * 2);
        NextGrid = new int[Columns, Rows];
        staticCells = new int[Columns, Rows];
        maxCells = Columns * Rows;
        int random = Random.Range(0, 2);
        Make2DGrid(ref Grid, Columns, Rows);
    }

    void Make2DGrid(ref Cell[,] cell, int columns, int rows)
    {
        cell = new Cell[columns, rows];
        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                int state = 1-Mathf.RoundToInt(heightmap.GetPixel((i + (Vertical / 10)+20) * (heightmap.width / columns), (j + (Vertical / 10)) * (heightmap.height / rows)).grayscale);
                cell[i, j] = new Cell(state, i, j, prefab);
                if (state == 1)
                    staticCells[i, j] = 1;
            }
        }
    }

    float stateAverage = 0;
    private void Update()
    {
       // stateAverage = 0;
        for (int i = 0; i < Columns; i++)
        {
            for (int j = 0; j < Rows; j++)
            {
                tempCell = Grid[i, j];
                if (tempCell.State == 0 && tempCell.Renderer.enabled)
                    tempCell.Renderer.enabled = false;
                else if (tempCell.State == 1 && !tempCell.Renderer.enabled)
                {
                  //  stateAverage++;
                    tempCell.Renderer.enabled = true;
                }
            }
        }
        //print((stateAverage / maxCells)*30);
      //  a_Source.pitch = 2 + ((stateAverage / maxCells) * 30);
       // a_Source.Play();


        for (int i = 0; i < Columns; i++)
        {
            for (int j = 0; j < Rows; j++)
            {
                int state = Grid[i, j].State;

                if (staticCells[i, j] == 0)
                {
                    int neighboors = CountNeighboors(Grid, i, j);


                    if (state == 0 && neighboors == 3)
                    {
                        NextGrid[i, j] = 1;
                    }
                    else if (state == 1 && (neighboors < 2 || neighboors > 3))
                    {
                        NextGrid[i, j] = 0;
                    }
                    else
                    {
                        NextGrid[i, j] = state;
                    }
                }
                else
                    NextGrid[i, j] = state;

            }
        }
        for (int i = 0; i < Columns; i++)
        {
            for (int j = 0; j < Rows; j++)
            {
                int state = Grid[i, j].State;
                int nextState = NextGrid[i, j];
                if (state != nextState)
                {
                    Grid[i, j].SetState(nextState);
                }
                else
                {
                    if (staticCells[i, j] == 0 && Time.time - Grid[i, j].timeChanged > Random.Range(1, 200) )
                    {
                        int n = CountNeighboors(Grid, i, j);
                        if (n >= 1)
                            Grid[i, j].FlipState();
                    }
                }
            }
        }
        //Grid = next;
    }

    int CountNeighboors(Cell[,] grid, int x, int y)
    {
        int sum = 0;
        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                var col = (x + i + Columns) % Columns;
                var row = (y + j + Rows) % Rows;


                sum += grid[col, row].State;
            }
        }
        return sum - grid[x, y].State;
    }
}
